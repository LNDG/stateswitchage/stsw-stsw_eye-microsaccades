% Analyze pupil diameter in the different conditions, here cut based on
% stimulus presentation period

% For z-normalized version, normalization is done within run across all
% time points to control for interindividual baseline differences without
% biasing condition differences within subject.

% 180117 | written for STSW dynamic (adapted from MD)
% 180131 | paths edited, cleaned up, added blink interpolation
%        | changed automatic artifact detection to 2nd derivative of left
%        eye position
%        | added z-score and demeaned version across TOIs of runs
% 180228 | removed behavioral data, required only during plotting
%        | replaced z-score with z-score NaN version
%        | corrected ID range
%        | create dedicated cfg_eye structure with parameters
% 181119 | combined YA + OA
%        | encode in FieldTrip-like structure
% 190712 | preserve original Y Gaze vector

clear all; clc; restoredefaultpath

% N = 47 YA, N = 53 (excluding pilots); % Errors for 1223 runs 2 through 4
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'; ...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/';
pn.fieldtrip        = [pn.root, 'eye/SA_EEG/B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.shadedError      = [pn.root, 'eye/SA_EEG/B_analyses/A_pupilDilation/T_tools/shadedErrorBar/']; addpath(pn.shadedError);
pn.naninterp        = [pn.root, 'eye/SA_EEG/B_analyses/A_pupilDilation/T_tools/naninterp/']; addpath(pn.naninterp);
pn.behavior         = [pn.root, 'behavior/STSW_dynamic/A_MergeIndividualData/B_data/'];
pn.eyeDataEEG       = [pn.root, 'eeg/task/A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.eyeDataMrk       = [pn.root, 'eeg/task/A_preproc/SA_preproc_study/B_data/B_EEG_ET_ByRun/'];
pn.DataOut          = [pn.root, 'eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/'];

% load behavioral data for condition information etc.
load([pn.behavior, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'IDs_all')
BehavioralIDs = IDs_all; clear IDs_all;

dataStim = NaN(numel(IDs),4,64,7000);
dataBlocks = [];
for indID = 1:numel(IDs)
    for indRun = 1:4
        try
            disp([IDs{indID},'_r',num2str(indRun)]);

            load([pn.eyeDataEEG, IDs{indID},'_r',num2str(indRun),'_dynamic_eyeEEG_Raw'], 'data_eyeEEG');
            load([pn.eyeDataMrk, IDs{indID},'_r',num2str(indRun),'_dynamic_mrk_eyeEEG.mat'], 'event', 'urevent');
            %load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG_YA/A_preprocessing/B_data/D_eye_mat_edited/S',IDs{indID},'r',num2str(indRun),'.mat']);

            cfg_eye.AddBlinkLength = 150; % 150 ms surrounding blinks
            cfg_eye.ArtThreshold = 1000;
            cfg_eye.zvaluecutoff = 3;
            cfg_eye.debug = 0;
            cfg_eye.mark_vert_automatic = 1;

            data_eye = data_eyeEEG; clear data_eyeEEG;

            %% Art correction: marking blinks using the pupil channel:
            % create cell array containing artifacts for each trial
            % config.trl is used to indicate the trial onsets
            for i = 1:length(data_eye.trial)
                A = (1:1:length(data_eye.trial{1,i}));

                % exclude / mark as art trials with a pupil value below a fixed
                % threshold
                if ~cfg_eye.debug
                    IndBlink = data_eye.trial{1,i}(4,:)<cfg_eye.ArtThreshold;
                else
                    IndBlink = data_eye.trial{1,i}(4,:)<-100;
                end
                A        = A(IndBlink)';
                % add constant to correct timing info
                A        = A+length(data_eye.trial{1,1})*(i-1);

                art{1,i} = A;
            end

            % combine artifact information in one vector
            cat_art = [];
            for i = 1:numel(art)
                cat_art = [cat_art; art{i}];
            end

            %% marking occluded pupil / eye movements using vertical eye channel
            
            % compute first derivative of Y-movements
            targetChan = find(strcmp(data_eye.label, 'L-GAZE-Y'));
                        
            data_eye.trial{1}(size(data_eye.trial{1},1)+1,:) = [0, 0, abs(diff(diff(data_eye.trial{1}(targetChan,:))))];
            data_eye.label(end+1) = {'L-GAZE-Y-1stDiff'};
            
                cfg                     = [];
                cfg.continuous          = 'no';

                % channel selection, cutoff and padding
                cfg.artfctdef.zvalue.channel    = 'L-GAZE-Y-1stDiff';

                cfg.artfctdef.zvalue.cutoff     = cfg_eye.zvaluecutoff;
                cfg.artfctdef.zvalue.trlpadding = 0;
                cfg.artfctdef.zvalue.fltpadding = 0;
                cfg.artfctdef.zvalue.artfctpeakrange =[- cfg_eye.AddBlinkLength/1000 cfg_eye.AddBlinkLength/1000];
                cfg.artfctdef.zvalue.artfctpeak  = 'yes';

                % make the process interactive
                if cfg_eye.mark_vert_automatic == 0
                    cfg.artfctdef.zvalue.interactive = 'yes';
                else
                    cfg.artfctdef.zvalue.interactive = 'no';
                end

                try
                    [cfg, artifact_vert] = ft_artifact_zvalue(cfg, data_eye);
                    artifact_vert_peaks = cfg.artfctdef.zvalue.dssartifact;

                    if isempty(artifact_vert) || strcmp(artDetec_iterative, 'no')
                        artDetec_iterative_go = 0;
                    end
                    
                    % perform manual padding around detected art

                    artifact_vert(:,1) = artifact_vert(:,1) - cfg_eye.AddBlinkLength;
                    artifact_vert(:,2) = artifact_vert(:,2) + cfg_eye.AddBlinkLength;

                    warning('Reminder: Art padding set to +/- cfg_eye.AddBlinkLength')

                    % sometimes there are no art with the current parameter, use catch to avoid crashs
                catch
                    artifact_vert(1,1) = 1; artifact_vert(1,2) = 1;
                end

                % set negative sample values to 1;
                artifact_vert(find(artifact_vert(artifact_vert<=cfg_eye.AddBlinkLength))) = 1;

                %% exclude artifacts

                for indArt = 1:size(artifact_vert)
                    data_eye.trial{1}(:,artifact_vert(indArt,1):artifact_vert(indArt,2)) = NaN;
                end
            %% restrict to eye channels
            
            data_eye.label = data_eye.label(66:end);
            data_eye.trial{1} = data_eye.trial{1}(66:end,:);
            
            %% cut into stimuli (-3000 + 1000; 1000 Hz sampling rate)

            % S 20 to S24: stimulus onset to stimulus offset
            
            findMat = strfind({event.type}, 'S 20');
            idx = find(~cellfun(@isempty,findMat));
            triggerMatchingVals_stimOnset= [event(idx).latency]';

%             findMat = strfind({event.type}, 'S 24');
%             idx = find(~cellfun(@isempty,findMat));
%             triggerMatchingVals_stimOffset= [event(idx).latency]';
            
            cfg_eye.includeSamplesPreStim = 3500;
            cfg_eye.includeSamplesPostStim = 2000;
            
            for j = 1:size(triggerMatchingVals_stimOnset,1)
                cfg_eye.trl(j,1) = triggerMatchingVals_stimOnset(j) - cfg_eye.includeSamplesPreStim;
                cfg_eye.trl(j,2) = triggerMatchingVals_stimOnset(j) + 3000 + cfg_eye.includeSamplesPostStim;
                cfg_eye.trl(j,3) = - cfg_eye.includeSamplesPreStim;
            end; clear j
                        
            tmp_data_eye{indRun} = ft_redefinetrial(cfg_eye, data_eye);
            
        catch
            disp(['Error for ', IDs{indID},'_r',num2str(indRun)]);
        end
    end
    %% add trial infos 
    tmp_ID = ismember(BehavioralIDs,IDs(indID));
    pupilData.TrlInfo.StateOrders(indID,:) = MergedDataEEG.StateOrders(:,tmp_ID);
    pupilData.TrlInfo.Atts(indID,:) = MergedDataEEG.Atts(:,tmp_ID);
    pupilData.TrlInfo.RTs(indID,:) = MergedDataEEG.RTs(:,tmp_ID);
    pupilData.TrlInfo.Accs(indID,:) = MergedDataEEG.Accs(:,tmp_ID);
    %% concatenate across runs
    eyeData = ft_appenddata(cfg, tmp_data_eye{1}, tmp_data_eye{2}, tmp_data_eye{3}, tmp_data_eye{4});
    eyeData.TrlInfo = pupilData.TrlInfo;
%     figure; imagesc(squeeze(nanmean(cat(3,eyeData.trial{:}),3)));
    %% save subject structure
    save([pn.DataOut, IDs{indID}, '_eyeData.mat'], 'eyeData');
end
