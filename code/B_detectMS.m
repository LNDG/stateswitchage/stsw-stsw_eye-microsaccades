clear all; clc; restoredefaultpath

% N = 47 YA, N = 53 (excluding pilots); % Errors for 1223 runs 2 through 4
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'; ...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/';
pn.fieldtrip        = [pn.root, 'eye/SA_EEG/B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.eyeData          = [pn.root, 'eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/'];

for indID = 1:numel(IDs)
    %% load subject structure
    load([pn.eyeData, IDs{indID}, '_eyeData.mat'], 'eyeData');
    
    %% perform microsaccade detection
    
    for iTrial = 1:256
        eyeData.trial{iTrial}(isnan(eyeData.trial{iTrial})) = 0;
    end
    
    cfg = [];
    cfg.method = 'velocity2D';
    cfg.channel = {'L-VEL-X', 'L-VEL-Y'};
    cfg.velocity2D.demean = 'yes';
%     cfg.velocity2D.demean   = 'no' or 'yes', whether to apply centering correction (default = 'yes')
%     cfg.velocity2D.mindur   = minimum microsaccade durantion in samples (default = 3);
%     cfg.velocity2D.velthres
    [cfg movement] = ft_detect_movement_JQK(cfg, eyeData);
    
    move1 = cat(1, movement{eyeData.TrlInfo.StateOrders(indID,:) == 1});
    move2 = cat(1, movement{eyeData.TrlInfo.StateOrders(indID,:) == 2});
    move3 = cat(1, movement{eyeData.TrlInfo.StateOrders(indID,:) == 3});
    move4 = cat(1, movement{eyeData.TrlInfo.StateOrders(indID,:) == 4});
    
    clear movement;
    
%     figure; hold on;
%     histogram(move1(:,3), 30)
%     histogram(move2(:,3), 30)
%     histogram(move3(:,3), 30)
%     histogram(move4(:,3), 30)
%     legend({'L1', 'L2', 'L3', 'L4'})

    save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/B_microsaccades/B_data/A_velocity2Ddetection/',IDs{indID}, '_ms_ByLoad.mat'], 'move*', 'cfg');
    
end
