% collapse microsaccade detection results across subjects

clear all; clc; restoredefaultpath

% N = 47 YA, N = 53 (excluding pilots); % Errors for 1223 runs 2 through 4
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'; ...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

pn.root             = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/';
pn.fieldtrip        = [pn.root, 'eye/SA_EEG/B_analyses/A_pupilDilation/T_tools/fieldtrip-20170904/']; addpath(pn.fieldtrip); ft_defaults;
pn.eyeData          = [pn.root, 'eye/SA_EEG/A_preprocessing/B_data/E_eye_FT_cleaned/'];

MSStruct = [];
for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}]);
    %% load subject structure
    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/B_microsaccades/B_data/A_velocity2Ddetection/',IDs{indID}, '_ms_ByLoad.mat'], 'move*', 'cfg');
    [MSStruct.counts1(indID,:), MSStruct.edges] = histcounts(move1(:,1), 50);
    [MSStruct.counts2(indID,:), MSStruct.edges] = histcounts(move2(:,1), 50);
    [MSStruct.counts3(indID,:), MSStruct.edges] = histcounts(move3(:,1), 50);
    [MSStruct.counts4(indID,:), MSStruct.edges] = histcounts(move4(:,1), 50);
end

h = figure('units','normalized','position',[.1 .1 .5 .5]);
subplot(3,2,1), imagesc(MSStruct.edges, [], MSStruct.counts1, [0 100]); xlabel('Time bin rel. to Stim On (s)'); ylabel('Subjects'); title('1 Target')
subplot(3,2,2), imagesc(MSStruct.edges, [], MSStruct.counts2, [0 100]); xlabel('Time bin rel. to Stim On (s)'); ylabel('Subjects'); title('2 Targets')
subplot(3,2,3), imagesc(MSStruct.edges, [], MSStruct.counts3, [0 100]); xlabel('Time bin rel. to Stim On (s)'); ylabel('Subjects'); title('3 Targets')
subplot(3,2,4), imagesc(MSStruct.edges, [], MSStruct.counts4, [0 100]); xlabel('Time bin rel. to Stim On (s)'); ylabel('Subjects'); title('4 Targets')
subplot(3,2,[5,6]); hold on;
plot(MSStruct.edges(1:end-1),mean(MSStruct.counts1,1))
plot(MSStruct.edges(1:end-1),mean(MSStruct.counts2,1))
plot(MSStruct.edges(1:end-1),mean(MSStruct.counts3,1))
plot(MSStruct.edges(1:end-1),mean(MSStruct.counts4,1))
legend({'L1', 'L2', 'L3', 'L4'}); legend('boxoff')
xlabel('Time bin rel. to Stim On (s)'); ylabel('Microsaccades per bin'); title('Average across subjects');
set(findall(gcf,'-property','FontSize'),'FontSize',20)

h = figure('units','normalized','position',[.1 .1 .3 .3]);
hold on;
plot(MSStruct.edges(1:end-1),mean((squeeze(nanmean(cat(3,MSStruct.counts2, MSStruct.counts3,MSStruct.counts4),3))-MSStruct.counts1),1), 'LineWidth', 2)
%legend({'L1', 'L2', 'L3', 'L4'}); legend('boxoff')
xlabel('Time bin rel. to Stim On (s)'); ylabel('Microsaccades per bin'); title('Average across subjects');
set(findall(gcf,'-property','FontSize'),'FontSize',20)


pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/B_microsaccades/C_figures/';
figureName = 'C_velocity2D_grandAverage';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% plot cumulative microsaccade rate
idxStimOnset = find(MSStruct.edges>=0, 1, 'first');
idxStimOffset = find(MSStruct.edges>=3, 1, 'first');

figure; hold on;
plot(MSStruct.edges(idxStimOnset:end-1),mean(cumsum(MSStruct.counts1(:,idxStimOnset:end),2),1))
plot(MSStruct.edges(idxStimOnset:end-1),mean(cumsum(MSStruct.counts2(:,idxStimOnset:end),2),1))
plot(MSStruct.edges(idxStimOnset:end-1),mean(cumsum(MSStruct.counts3(:,idxStimOnset:end),2),1))
plot(MSStruct.edges(idxStimOnset:end-1),mean(cumsum(MSStruct.counts4(:,idxStimOnset:end),2),1))

figure;
barToPlot = [mean(sum(MSStruct.counts1(:,idxStimOnset:idxStimOffset),2)), ...
    mean(sum(MSStruct.counts2(:,idxStimOnset:idxStimOffset),2)),...
    mean(sum(MSStruct.counts3(:,idxStimOnset:idxStimOffset),2)),...
    mean(sum(MSStruct.counts4(:,idxStimOnset:idxStimOffset),2))];
bar(barToPlot)

figure;
barToPlot = [std(sum(MSStruct.counts1(:,idxStimOnset:idxStimOffset),2)), ...
    std(sum(MSStruct.counts2(:,idxStimOnset:idxStimOffset),2)),...
    std(sum(MSStruct.counts3(:,idxStimOnset:idxStimOffset),2)),...
    std(sum(MSStruct.counts4(:,idxStimOnset:idxStimOffset),2))];
bar(barToPlot)

figure; imagesc([sum(MSStruct.counts1(:,idxStimOnset:idxStimOffset),2), ...
    sum(MSStruct.counts2(:,idxStimOnset:idxStimOffset),2),...
    sum(MSStruct.counts3(:,idxStimOnset:idxStimOffset),2),...
    sum(MSStruct.counts4(:,idxStimOnset:idxStimOffset),2)])

[h, p] = ttest(nanmean(MSStruct.counts1(:,idxStimOnset:idxStimOffset),2), ...
    squeeze(nanmean([nanmean(MSStruct.counts2(:,idxStimOnset:idxStimOffset),2), ...
    nanmean(MSStruct.counts3(:,idxStimOnset:idxStimOffset),2),...
    nanmean(MSStruct.counts4(:,idxStimOnset:idxStimOffset),2)],2)))